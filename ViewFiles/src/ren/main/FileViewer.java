package ren.main;

import java.sql.ResultSet;
import java.sql.Statement;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import ren.errors.ModuleNotSupported;
import ren.model.StorageDriver;
import com.amazonaws.services.lambda.runtime.Context;
import java.sql.SQLException;
import ren.errors.DBException;
import java.sql.DriverManager;
import java.sql.Connection;
import ren.model.StorageModule;
import ren.model.Event;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class FileViewer implements RequestHandler<Event, StorageModule>
{
    private static final String DBURL = "jdbc:postgresql://synctruck-database-instance-1.cyusxcvzomzn.us-east-2.rds.amazonaws.com:5432/synctruck";
    private static final String DBUSERNAME = "renato";
    private static final String DBPASSWORD = "mipinguino1";
    public static final Connection con;
    
    private static Connection connectDB() {
        try {
            String DBURL = System.getenv("PGHOST");
            String DBUSERNAME = System.getenv("PGUSER");
            String DBPASSWORD = System.getenv("PGPASSWORD");
            String DBDATABASE = System.getenv("PGDATABASE");
            String connString = "jdbc:postgresql://"+DBURL+":5432/"+DBDATABASE;
            return DriverManager.getConnection(connString,DBUSERNAME,DBPASSWORD);
        }
        catch (SQLException sqlexcp) {
            throw new DBException("Connection Failure");
        }
    }
    
    public StorageModule handleRequest(final Event event, final Context context) {
        final LambdaLogger logger = context.getLogger();
        final String query = event.getQuery();
        try {
            logger.log(String.format("Query: %s", query));
            final Statement st = FileViewer.con.createStatement();
            final ResultSet rs = st.executeQuery(query);
            final String module = event.module;
            switch (module) {
                case "DRIVER": {
                    final StorageDriver res = new StorageDriver(event.id);
                    res.setFiles(rs);
                    return res;
                }
                default: {
                    throw new ModuleNotSupported(event.module);
                }
            }
        }
        catch (SQLException excp) {
            logger.log("FAILED QUERY: " + query);
            throw new DBException(excp.getMessage());
        }
    }
    
    static {
        con = connectDB();
    }
}
