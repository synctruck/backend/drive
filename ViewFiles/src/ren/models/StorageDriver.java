package ren.model;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import ren.errors.DBException;
import ren.main.FileViewer;

public class StorageDriver extends StorageModule
{
    public StorageDriver(final int id) {
        this.id = id;
        final String query = String.format("select * from driver_file_overview where id = %s", id);
        try {
            final Statement st = FileViewer.con.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                this.title = rs.getString(2);
                this.Overview = new String[] { rs.getString("files_count"), rs.getString("total_size"), rs.getString("last_upload"), rs.getString("image_count"), rs.getString("document_count") };
                this.profile = rs.getString("profile");
            }
        }
        catch (SQLException excp) {
            throw new DBException(excp.getMessage());
        }
    }
}
