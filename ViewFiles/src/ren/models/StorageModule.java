package ren.model;

import java.sql.SQLException;
import ren.errors.DBException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class StorageModule
{
    public String[] Overview;
    public int id;
    public String profile;
    public String title;
    public ArrayList<StorageFile> files;
    
    public void setFiles(final ResultSet rs) {
        this.files = new ArrayList<StorageFile>();
        try {
            while (rs.next()) {
                this.files.add(new StorageFile(rs));
            }
        }
        catch (SQLException sqlE) {
            throw new DBException(sqlE.getMessage());
        }
    }
}
