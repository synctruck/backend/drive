package ren.model;

import java.sql.SQLException;
import ren.errors.DBException;
import java.sql.ResultSet;

public class StorageFile
{
    public String name;
    public String type;
    public String size;
    public String date_uploaded;
    public String uploaded_by;
    public String url;
    
    public StorageFile() {
    }
    
    public StorageFile(final ResultSet rs) {
        try {
            this.name = rs.getString(1);
            this.type = rs.getString(2);
            this.size = rs.getString(3);
            this.date_uploaded = rs.getString(4);
            this.uploaded_by = rs.getString(5);
            this.url = rs.getString(6);
        }
        catch (SQLException sqlE) {
            throw new DBException(sqlE.getMessage());
        }
    }
}
