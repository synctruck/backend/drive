// 
// Decompiled by Procyon v0.5.36
// 

package ren.errors;

public class DBException extends RuntimeException
{
    public DBException(final String query) {
        super("Failed query: " + query);
    }
}
