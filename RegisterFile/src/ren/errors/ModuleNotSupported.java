// 
// Decompiled by Procyon v0.5.36
// 

package ren.errors;

public class ModuleNotSupported extends RuntimeException
{
    public ModuleNotSupported(final String module) {
        super("Module: " + module + " not supported for File Management");
    }
}
