// 
// Decompiled by Procyon v0.5.36
// 

package ren.model;

import ren.errors.ModuleNotSupported;

public class Event
{
    public String module;
    public int id;
    public int session;
    public String file;
    public String type;
    public int size;
    public boolean profile;
    
    public String getQuery() {
        final String queryParams = String.format("(%s,%s,%s,'%s','%s','%s',%s)", this.session, this.id, this.size, this.file, this.type, this.module, this.profile);
        if (!this.module.equals("DRIVER")) {
            throw new ModuleNotSupported(this.module);
        }
        return String.format("select * from register_file%s", queryParams);
    }
}
