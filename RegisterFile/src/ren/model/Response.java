// 
// Decompiled by Procyon v0.5.36
// 

package ren.model;

import java.net.URL;

public class Response
{
    public URL url;
    public String key;
    
    public Response() {
    }
    
    public Response(final URL u, final String name) {
        this.url = u;
        this.key = name;
    }
}
